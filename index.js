var _ = require('lodash'),
    fs = require('fs'),
    csv = require('csv');

module.exports = {

  terms: {},

  /**
   * load terms from CSV file..
   */
  loadTerms: function (file, cb) {
    var self = this,
        parse = csv.parse(function (err, data) {
          var i = data.length;
          while (i--) {
            self.terms[data[i][0]] = data[i][1];
          }
          cb();
        });
    fs.createReadStream(file).pipe(parse);
  }

};