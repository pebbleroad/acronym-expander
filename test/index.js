var should = require('chai').should(),
    expander = require('../index');

describe('#expand terms', function() {

  before(function() {
    var terms = {};
    terms.ABC = 'Eh Bee See';
    terms.XYZ = 'Hex Why Zeed';
    expander.terms = terms;
  });

  it('should have two terms loaded', function() {
    expander.terms.ABC.should.equal('Eh Bee See');
    expander.terms.XYZ.should.equal('Hex Why Zeed');
  });
});